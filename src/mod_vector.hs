module Main where

mod_vec_and_degree x y = [sqrt (x**2 + y**2), (atan (y/x)) * (360/(pi*2)), (atan (y/x)) * (360/(pi*2)) + 180]
vec_from_points x1 y1 x2 y2 = [x2-x1, y2-y1]

main = do
    print (mod_vec_and_degree (-2) (-2))
    print (mod_vec_and_degree (-2) (2))
    print (mod_vec_and_degree (2) (2))
    print (mod_vec_and_degree (2) (-2))
    print (vec_from_points 1 2 3 6)
    print (vec_from_points 2 5 10 6)
    print (vec_from_points 4 2 8 6)
    print (vec_from_points 10 2 6 3)
