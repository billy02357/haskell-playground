lastButN n (x:xs) = if (length xs) == n
                    then x
                    else lastButN (n) xs
