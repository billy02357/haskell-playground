-- char count program
main = interact charCount
    where charCount input = show (length input) ++ "\n"
