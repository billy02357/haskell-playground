data Cartesian2D = Cartesian2D { x :: Double,
                                 y :: Double
                               } deriving (Eq, Show)

data Polar2D = Polar2D { len :: Double,
                         alpha :: Double
                       } deriving (Eq, Show)

type Degree = Double
type Length = Double

to_rad n = n * (pi*2)/360
to_degree n = n * 360/(pi*2)

len_cart :: Cartesian2D -> Length
alpha_cart :: Cartesian2D -> Degree

len_cart vec = sqrt (((x vec)**2) + ((y vec)**2))
alpha_cart vec = to_degree (atan ((y vec)/(x vec)))


pol_x :: Polar2D -> Double
pol_y :: Polar2D -> Double

pol_x vec = len vec * cos (to_rad (alpha vec))
pol_y vec = len vec * sin (to_rad (alpha vec))
